import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JSeparator;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import javax.swing.ButtonGroup;
import javax.swing.JProgressBar;
import java.awt.Toolkit;
import javax.swing.JButton;
import java.awt.Color;

public class ventana extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField txtDiamesao;
	private JPasswordField passwordField;
	private JPasswordField passwordField_1;
	private JTextField textField_4;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField textField_5;
	private JTextField textField_6;
	private JPasswordField passwordField_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ventana frame = new ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ventana() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("/root/Imágenes/9.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnInicio = new JMenu("Inicio");
		menuBar.add(mnInicio);
		
		JMenuItem mntmRegistro = new JMenuItem("Registro");
		mnInicio.add(mntmRegistro);
		
		JMenuItem mntmSesin = new JMenuItem("Sesion");
		mnInicio.add(mntmSesin);
		
		JMenuItem mntmPago = new JMenuItem("Pago");
		mnInicio.add(mntmPago);
		
		JMenuBar menuBar_1 = new JMenuBar();
		mnInicio.add(menuBar_1);
		
		JMenu mnDesacargas = new JMenu("Desacargas");
		menuBar.add(mnDesacargas);
		
		JMenuItem mntmMisDescargas = new JMenuItem("Mis Descargas");
		mnDesacargas.add(mntmMisDescargas);
		
		JMenuItem mntmRecomendadas = new JMenuItem("Recomendadas");
		mnDesacargas.add(mntmRecomendadas);
		
		JMenu mnVentana = new JMenu("Ventana");
		menuBar.add(mnVentana);
		
		JMenuItem mntmApariencia = new JMenuItem("Apariencia");
		mnVentana.add(mntmApariencia);
		
		JMenu mnGenero = new JMenu("Genero");
		menuBar.add(mnGenero);
		
		JMenuItem mntmAccion = new JMenuItem("Accion");
		mnGenero.add(mntmAccion);
		
		JMenuItem mntmTerror = new JMenuItem("Terror");
		mnGenero.add(mntmTerror);
		
		JMenuItem mntmSuspense = new JMenuItem("Suspense");
		mnGenero.add(mntmSuspense);
		
		JMenuItem mntmCienciaFiccion = new JMenuItem("Ciencia Ficcion");
		mnGenero.add(mntmCienciaFiccion);
		
		JMenuItem mntmInfantil = new JMenuItem("Infantil");
		mnGenero.add(mntmInfantil);
		
		JMenuItem mntmComedia = new JMenuItem("Comedia");
		mnGenero.add(mntmComedia);
		
		JMenuItem mntmRomantica = new JMenuItem("Romantica");
		mnGenero.add(mntmRomantica);
		
		JMenuItem mntmFantasia = new JMenuItem("Fantasia");
		mnGenero.add(mntmFantasia);
		
		JMenu mnAyuda = new JMenu("Ayuda");
		menuBar.add(mnAyuda);
		
		JMenuItem mntmSoporteTcnico = new JMenuItem("Soporte Tecnico");
		mnAyuda.add(mntmSoporteTcnico);
		
		JMenuItem mntmVerisonApp = new JMenuItem("Verison app");
		mnAyuda.add(mntmVerisonApp);
		
		JMenuItem mntmPluginextensiones = new JMenuItem("Plug-In/extensiones");
		mnAyuda.add(mntmPluginextensiones);
		
		JMenuItem mntmApoyo = new JMenuItem("Apoyo");
		mnAyuda.add(mntmApoyo);
		
		JMenu mnBusqueda = new JMenu("Busqueda");
		menuBar.add(mnBusqueda);
		
		JMenuItem mntmExplorar = new JMenuItem("Explorar");
		mnBusqueda.add(mntmExplorar);
		
		JLabel lblBuscar = new JLabel("Buscar:");
		mnBusqueda.add(lblBuscar);
		
		textField = new JTextField();
		mnBusqueda.add(textField);
		textField.setColumns(10);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Registro", null, panel, null);
		panel.setLayout(null);
		
		JLabel lblApellidos = new JLabel("Apellidos:");
		lblApellidos.setBounds(12, 12, 78, 15);
		panel.add(lblApellidos);
		
		textField_1 = new JTextField();
		textField_1.setBounds(88, 10, 124, 19);
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(224, 12, 66, 15);
		panel.add(lblNombre);
		
		textField_2 = new JTextField();
		textField_2.setBounds(295, 10, 124, 19);
		panel.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblCorreo = new JLabel("Correo:");
		lblCorreo.setBounds(12, 39, 66, 15);
		panel.add(lblCorreo);
		
		textField_3 = new JTextField();
		textField_3.setBounds(88, 39, 124, 19);
		panel.add(textField_3);
		textField_3.setColumns(10);
		
		JLabel lblEdad = new JLabel("Edad:");
		lblEdad.setBounds(224, 39, 66, 15);
		panel.add(lblEdad);
		
		JSpinner spinner = new JSpinner();
		spinner.setBounds(269, 37, 36, 20);
		panel.add(spinner);
		
		JLabel lblPais = new JLabel("Pais:");
		lblPais.setBounds(12, 92, 66, 15);
		panel.add(lblPais);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Espa�a", "Estados Unidos", "Rusia", "Francia", "Israel"}));
		comboBox.setBounds(88, 87, 124, 24);
		panel.add(comboBox);
		
		JLabel lblNacimiento = new JLabel("Nacimiento:");
		lblNacimiento.setBounds(223, 92, 95, 15);
		panel.add(lblNacimiento);
		
		txtDiamesao = new JTextField();
		txtDiamesao.setText("dia/mes/a�o");
		txtDiamesao.setBounds(315, 90, 95, 19);
		panel.add(txtDiamesao);
		txtDiamesao.setColumns(10);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(22, 93, 1, 2);
		panel.add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(98, 97, 1, 2);
		panel.add(separator_1);
		
		JLabel lblContrasea = new JLabel("Contrase�a:");
		lblContrasea.setBounds(12, 119, 95, 15);
		panel.add(lblContrasea);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(98, 117, 114, 19);
		panel.add(passwordField);
		
		JLabel lblRepita = new JLabel("Repita su Contrase�a:\n");
		lblRepita.setBounds(12, 146, 159, 15);
		panel.add(lblRepita);
		
		passwordField_1 = new JPasswordField();
		passwordField_1.setBounds(170, 144, 135, 19);
		panel.add(passwordField_1);
		
		JRadioButton rdbtnDeseoRecibirNotificaciones = new JRadioButton("Deseo recibir notificaciones por correo");
		rdbtnDeseoRecibirNotificaciones.setBounds(12, 192, 293, 23);
		panel.add(rdbtnDeseoRecibirNotificaciones);
		
		JLabel lblMovil = new JLabel("Movil:");
		lblMovil.setBounds(224, 117, 66, 15);
		panel.add(lblMovil);
		
		textField_4 = new JTextField();
		textField_4.setBounds(295, 113, 124, 19);
		panel.add(textField_4);
		textField_4.setColumns(10);
		
		JLabel lblSexo = new JLabel("Sexo:");
		lblSexo.setBounds(224, 62, 66, 15);
		panel.add(lblSexo);
		
		JCheckBox chckbxH = new JCheckBox("H");
		buttonGroup.add(chckbxH);
		chckbxH.setBounds(269, 62, 36, 15);
		panel.add(chckbxH);
		
		JCheckBox chckbxM = new JCheckBox("M");
		buttonGroup.add(chckbxM);
		chckbxM.setBounds(305, 62, 46, 15);
		panel.add(chckbxM);
		
		JRadioButton rdbtnDeseoMostrarMi = new JRadioButton("Deseo mostrar mi cumplea�os en la plataforma");
		rdbtnDeseoMostrarMi.setBounds(12, 165, 347, 23);
		panel.add(rdbtnDeseoMostrarMi);
		
		JLabel lblNewLabel = new JLabel("Usuario:\n");
		lblNewLabel.setBounds(12, 66, 78, 15);
		panel.add(lblNewLabel);
		
		textField_5 = new JTextField();
		textField_5.setBounds(88, 60, 124, 19);
		panel.add(textField_5);
		textField_5.setColumns(10);
		
		JButton btnAceptar = new JButton("ACEPTAR");
		btnAceptar.setBounds(315, 191, 116, 25);
		panel.add(btnAceptar);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Sesion", null, panel_1, null);
		panel_1.setLayout(null);
		
		JLabel lblNombreDeUsuario = new JLabel("Nombre de Usuario");
		lblNombreDeUsuario.setBounds(12, 78, 150, 15);
		panel_1.add(lblNombreDeUsuario);
		
		textField_6 = new JTextField();
		textField_6.setBounds(180, 76, 186, 19);
		panel_1.add(textField_6);
		textField_6.setColumns(10);
		
		JLabel lblContrasea_1 = new JLabel("Contrase�a:");
		lblContrasea_1.setBounds(12, 105, 150, 15);
		panel_1.add(lblContrasea_1);
		
		passwordField_2 = new JPasswordField();
		passwordField_2.setBounds(180, 103, 186, 19);
		panel_1.add(passwordField_2);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setIndeterminate(true);
		progressBar.setBounds(271, 189, 148, 14);
		panel_1.add(progressBar);
		
		JButton btnAceptar_1 = new JButton("Aceptar");
		btnAceptar_1.setBounds(12, 178, 114, 25);
		panel_1.add(btnAceptar_1);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBackground(Color.RED);
		btnCancelar.setBounds(145, 178, 114, 25);
		panel_1.add(btnCancelar);
		
		JButton btnHeOlvidadoMi = new JButton("He olvidado mi Contrase�a...");
		btnHeOlvidadoMi.setBounds(180, 134, 239, 25);
		panel_1.add(btnHeOlvidadoMi);
	}
}
